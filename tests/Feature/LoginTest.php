<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /** @test */
    public function it_visit_page_of_login()
    {
        $response = $this->get('/');

        $response->assertStatus(200)
                ->assertSee('Login');
    }

    /** @test */
    public function it_cant_register()
    {
        $response = $this->get('/');

        $response->assertStatus(200)
                ->assertDontSee('Register');
    }

}
