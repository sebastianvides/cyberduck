<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();
Auth::routes(['register' => false]);
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

Route::group(['middleware' => 'auth'], function(){

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/company',[
        'as'   => 'company.index',
        'uses' => 'CompanyController@index'
    ]);

    Route::get('/company/create',[
        'as'   => 'company.create',
        'uses' => 'CompanyController@create'
    ]);

    Route::post('/company/store',[
        'as'   => 'company.store',
        'uses' => 'CompanyController@store'
    ]);

    Route::get('/company/show/{id}',[
        'as'   => 'company.show',
        'uses' => 'CompanyController@show'
    ]);

    Route::get('/company/delete/{id}',[
        'as'   => 'company.delete',
        'uses' => 'CompanyController@delete'
    ]);

    Route::post('/company/update/{id}',[
        'as'   => 'company.update',
        'uses' => 'CompanyController@update'
    ]);


    Route::get('/employee',[
        'as'   => 'employee.index',
        'uses' => 'EmployeeController@index'
    ]);

    Route::get('/employee/create',[
        'as'   => 'employee.create',
        'uses' => 'EmployeeController@create'
    ]);

    Route::post('/employee/store',[
        'as'   => 'employee.store',
        'uses' => 'EmployeeController@store'
    ]);

    Route::get('/employee/show/{id}',[
        'as'   => 'employee.show',
        'uses' => 'EmployeeController@show'
    ]);

    Route::get('/employee/delete/{id}',[
        'as'   => 'employee.delete',
        'uses' => 'EmployeeController@delete'
    ]);

    Route::post('/employee/update/{id}',[
        'as'   => 'employee.update',
        'uses' => 'EmployeeController@update'
    ]);

});
