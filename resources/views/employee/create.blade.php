@extends('admin_template')

@section('content')

@include('partials.errors')

<form class="form-horizontal" method="POST" action="{{ route('employee.store') }}"
id="form_employee">
    {{ csrf_field() }}

    <div  style="padding-left: 50px; padding-bottom: 50px;">

        <fieldset width="100%">


            <legend>Employee</legend>

            <div class="block">
                <label for="firstName">First name (*)</label>
                <input type="text" name="firstName" id="firstName" value="{{ old('firstName') }}" size="30" required>
            </div>

            <div class="block">
                <label for="lastName">Last name (*)</label>
                <input type="text" name="lastName" id="lastName" value="{{ old('lastName') }}" size="30" required>
            </div>

            <div class="block">
                <label for="company">Company (*)</label>
                <select name="company" id="company">
                    <option value=""    {{ old('company') == ''    ? 'selected' : '' }}></option>
                    @foreach ($companies as $company)
                        <option value={!! $company->id !!}    {{ old('company') == $company->name   ? 'selected' : '' }}>{!! $company->name !!}</option>
                    @endforeach
                </select>
            </div>

            <div class="block">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" value="{{ old('email') }}" size="50">
            </div>

            <div class="block">
                <label for="phone">Phone</label>
                <input type="tel" name="phone" id="phone" value="{{ old('phone') }}" size="50">
            </div>

            </br>

            <div style="padding-left: 20px;">
                <p style="color:red;"><strong>(*) marked fields are mandatory</strong></p>
            </div>

            </br>

            <div class="block" align="center">
                <button type="submit">Create Employee</button>
            </div>

        </fieldset>

    </div>

</form>

@endsection
