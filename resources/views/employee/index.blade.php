@extends('admin_template')

@section('content')

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Employees
                </h1>
                <form method="GET" role="form" action=" {{ route('employee.create') }}">
                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-default" aria-label="Agregar">Add
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </button>
                </form>
                <!--
                <ol class="breadcrumb">

                </ol>
                -->
            </div>
        </div>
        <!-- /.row -->
        <div class="row">

            <div class="col-lg-12">

                @include('partials/errors')

                <h2></h2>
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="25%">Name</th>
                                <th width="25%">Company</th>
                                <th width="25%">Email</th>
                                <th width="15%">Phone</th>
                                <th width="10%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td>
                                    {{ $employee->fullName }}
                                </td>
                                <td>
                                    <img src="{{ ("/storage".$employee->worksAt->logo) }}" width="50" height="50">{{ ' '.$employee->worksAt->name }}
                                </td>
                                <td>
                                    {{ $employee->email }}
                                </td>
                                <td>
                                    {{ $employee->phone }}
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <form class="form-horizontal" method="GET" action="{{ route('employee.show', $employee->id) }}" id="form_employee">
                                                    {{ csrf_field() }}

                                                    <button type="submit" class="btn btn-default" aria-label="Edit">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                                                            <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                                                          </svg>
                                                    </button>
                                                </form>
                                            </td>
                                            <td>

                                                <form class="form-horizontal" method="GET" action="{{ route('employee.delete', $employee->id) }}" id="form_employee" onsubmit='return confirm("Are you sure you want to delete this record?")'>
                                                    {{ csrf_field() }}

                                                    <button type="submit" class="btn btn-default" aria-label="Delete">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                                        </svg>                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
    {{ $employees->appends(Request::input())->links() }}

</div>
<!-- /#page-wrapper -->
@endsection
