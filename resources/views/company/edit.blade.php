@extends('admin_template')

@section('content')

@include('partials.errors')

<form class="form-horizontal" method="POST" action="{{ route('company.update', $company->id) }}"
    id="form_company" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div  style="padding-left: 50px; padding-bottom: 50px;">

        <fieldset width="100%">

            <legend>Company</legend>

            <div class="block">
                <label for="logo_old">Company logo (*)</label>
                <img src="{{ ("/storage".$company->logo) }}" width="100" height="100">
            </div>

            <div class="block">
                <label for="name">Company name (*)</label>
                <input type="text" name="name" id="name" value="{{ old('name') ? old('name') : $company->name }}" size="30" required>
            </div>

            <div class="block">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" value="{{ old('email') ? old('email') : $company->email }}" size="50">
            </div>

            <div class="block">
                <label for="website">Website</label>
                <input type="url" name="website" id="website" value="{{ old('website') ? old('website') : $company->website }}" size="50">
            </div>

        </br>

        <legend>Change Logo</legend>

        <div class="block">
            <label for="logo">Upload your logo:</label>
            <input type="file" id="logo" name="logo" width="100px" multiple accept="image/*">
        </div>


        <div style="padding-left: 20px;">
            <p style="color:red;"><strong>(*) marked fields are mandatory</strong></p>
        </div>

        </br>

            <div class="block" align="center">
                <button type="submit">Update Company</button>
            </div>


        </fieldset>

    </div>


</form>

@endsection
