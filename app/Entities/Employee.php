<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable=['firstName','lastName','company','email','phone'];

    public function worksAt()
	{
		return $this->belongsTo(Company::class, 'company', 'id');
	}

    public function getFullNameAttribute()
    {
        return $this->lastName.', '.$this->firstName;
    }
}
