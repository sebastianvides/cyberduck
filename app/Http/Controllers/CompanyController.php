<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Entities\Company;
use Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Config;


class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::paginate(10);

        return view('company.index', compact('companies'));
    }

    public function create()
    {
        return view('company.create');
    }

    public function store(Request $request)
    {

        if ($request->hasFile('logo')) {

            $validator = $this->validate($request,[
                'name'  => 'required|max:200',
                'logo'  => 'image|mimes:jpeg,png,jpg,gif,svg,bmp|max:2048|dimensions:min_width=100,min_height=100'
            ]);

            $logo = $request->file('logo');
            $file = $logo.".".$logo->getClientOriginalExtension();

            Storage::disk('public')->put($file,  File::get($logo));

        }
        else{

            $validator = $this->validate($request,[
                'name'  => 'required|max:200',
            ]);

            $file=Config::get('parameters.options.noimage');
        }

        if(!$validator){
            return \Redirect::back()->withInput()->withErrors( $validator );
        }

        $company = Company::create([
            'name'     => $request->get('name'),
            'email'    => $request->get('email'),
            'website'  => $request->get('website'),
            'logo'     => $file,
        ]);

        session()->flash('success', 'Company added successfully');

        return Redirect::route('company.index');
    }

    public function show($id)
    {
        $company = Company::findOrFail($id);

        return view('company.edit', compact('company'));
    }

    public function update($id, Request $request)
    {

        $company = Company::findOrFail($id);

        if ($request->hasFile('logo')) {

            $validator = $this->validate($request,[
                'name'  => 'required|max:200',
                'logo'  => 'image|mimes:jpeg,png,jpg,gif,svg,bmp|max:2048|dimensions:min_width=100,min_height=100'
            ]);

            $logo = $request->file('logo');
            $file = $logo.".".$logo->getClientOriginalExtension();

            Storage::disk('public')->put($file,  File::get($logo));

        }
        else{

            $validator = $this->validate($request,[
                'name'  => 'required|max:200',
            ]);

            $file=$company->logo;
        }

        if(!$validator){
            return \Redirect::back()->withInput()->withErrors( $validator );
        }

        $company->update([
            'name'     => $request->get('name'),
            'email'    => $request->get('email'),
            'website'  => $request->get('website'),
            'logo'     => $file,
        ]);

        session()->flash('success', 'Company updated successfully');

        return Redirect::route('company.index');
    }

    public function delete($id)
    {

        $company = Company::findOrFail($id);

        if($company->employees->count()!=0) {
            session()->flash('danger', 'Error: Some employees works at that company. Delete them first.');
            return Redirect::route('company.index');
        }

        Storage::disk('public')->delete($company->logo);

        $company->delete();

        session()->flash('success', 'Company deleted successfully');

        return Redirect::route('company.index');
    }

}
