<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Entities\Employee;
use App\Entities\Company;
use Redirect;


class EmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::paginate(10);

        return view('employee.index', compact('employees'));
    }

    public function create()
    {
        $companies = Company::all();

        if($companies->count() == 0){
            session()->flash('danger', 'You have to create a company first.');
            return Redirect::route('employee.index');
        }

        return view('employee.create', compact('companies'));
    }

    public function store(Request $request)
    {

        $validator = $this->validate($request,[
            'firstName' => 'required|max:200',
            'lastName'  => 'required|max:200',
            'company'   => 'required|exists:companies,id'
        ]);

        if(!$validator){
            return \Redirect::back()->withInput()->withErrors( $validator );
        }

        $Employee = Employee::create([
            'firstName' => $request->get('firstName'),
            'lastName'  => $request->get('lastName'),
            'company'   => $request->get('company'),
            'email'     => $request->get('email'),
            'phone'     => $request->get('phone'),
        ]);

        session()->flash('success', 'Employee added successfully');

        return Redirect::route('employee.index');
    }

    public function show($id)
    {
        $employee = Employee::findOrFail($id);
        $companies = Company::all();

        return view('employee.edit', compact('employee','companies'));
    }

    public function update($id, Request $request)
    {

        $employee = Employee::findOrFail($id);

        $validator = $this->validate($request,[
            'firstName' => 'required|max:200',
            'lastName'  => 'required|max:200',
            'company'   => 'required|exists:companies,id'
        ]);

        if(!$validator){
            return \Redirect::back()->withInput()->withErrors( $validator );
        }

        $employee->update([
            'firstName' => $request->get('firstName'),
            'lastName'  => $request->get('lastName'),
            'company'   => $request->get('company'),
            'email'     => $request->get('email'),
            'phone'     => $request->get('phone'),
        ]);

        session()->flash('success', 'Employee updated successfully');

        return Redirect::route('employee.index');
    }

    public function delete($id)
    {

        Employee::findOrFail($id)->delete();

        session()->flash('success', 'Employee deleted successfully');

        return Redirect::route('employee.index');
    }

}
