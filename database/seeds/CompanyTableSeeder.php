<?php

use Illuminate\Database\Seeder;
use Faker\Generator;
use App\Entities\Company;

class CompanyTableSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createMultiple(20);
    }

    public function getModel()
    {
        return new Company();
    }

    public function getDummyData(Generator $faker, array $customValues = array())
    {
        return [
        		'name'			=> $faker->company(),
				'email'       	=> $faker->email(),
	            'logo'          => Config::get('parameters.options.noimage'), //$faker->image('public/storage/tmp',100,100, null, false),
	            'website'		=> $faker->url(),
        ];
    }
}

