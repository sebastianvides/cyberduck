<?php

use Illuminate\Database\Seeder;
use Faker\Generator;
use App\Entities\Employee;

class EmployeeTableSeeder extends BaseSeeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
       $this->createMultiple(100);
   }

   public function getModel()
   {
       return new Employee();
   }

   public function getDummyData(Generator $faker, array $customValues = array())
   {
       return [
               'firstName'		=> $faker->firstName(),
               'lastName'		=> $faker->lastName(),
               'email'       	=> $faker->email(),
               'company'        => $faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
               'phone'		    => $faker->phoneNumber(),
       ];
   }
}

